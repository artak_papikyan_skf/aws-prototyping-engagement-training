import pandas as pd
import numpy as np
import argparse
import os
from sklearn.model_selection import train_test_split
import boto3
import time


parser = argparse.ArgumentParser()
parser.add_argument('--region', type=str)
parser.add_argument('--group_id', type=str)
parser.add_argument('--event_time', type=float, default=1.638286637E9)
args = parser.parse_args()
region = args.region
group_id = args.group_id
event_time = args.event_time
boto3.setup_default_session(region_name=region)
athena = boto3.client("athena", region_name=region)
s3 = boto3.resource('s3')


athena_database_name = "sagemaker_featurestore"
bucket_name = "sagemaker-eu-west-1-676788233673"
table_name = "vsa-features-prototype-1637870612"

query_string = f"""
    SELECT *
    FROM (
            SELECT *,
                row_number() OVER (
                    PARTITION BY measurementid
                    ORDER BY eventtime desc,
                        Api_Invocation_Time DESC,
                        write_time DESC
                ) AS row_num
            FROM "{table_name}"
            where status is not null
                and group_id = '{group_id}'
        )
    WHERE row_num = 1
"""

# Send the query to the Athena engine and get query result URI
query_execution = athena.start_query_execution(
    QueryString=query_string,
    QueryExecutionContext={"Database": athena_database_name},
    ResultConfiguration={"OutputLocation": f"s3://{bucket_name}/artak/query_results/"},
)
query_execution_id = query_execution.get("QueryExecutionId")
query_details = athena.get_query_execution(QueryExecutionId=query_execution_id)
query_status = query_details["QueryExecution"]["Status"]["State"]

# Check if query result is ready
while query_status in ["QUEUED", "RUNNING"]:
    print(f"Query status: {query_status}")
    time.sleep(30)
    query_status = athena.get_query_execution(QueryExecutionId=query_execution["QueryExecutionId"])["QueryExecution"]["Status"]["State"]
print(f"status: {query_status}")


query_details = athena.get_query_execution(QueryExecutionId=query_execution_id)
query_result_s3_uri = (
    query_details.get("QueryExecution", {}).get("ResultConfiguration", {}).get("OutputLocation")
)
uri_split = query_result_s3_uri.split("/")
file_path = "/".join(uri_split[3:])

input_dir = "/opt/ml/processing/input"
input_data_path = os.path.join(input_dir, query_result_s3_uri.split('/')[-1])

# Downlaod Athena query result into the container input drectory
s3.Bucket(bucket_name).download_file(
    file_path,
    input_data_path
)

df = pd.read_csv(input_data_path)


def convert_string_to_float_array(s: str) -> np.ndarray:
    return np.array([float(i) for i in s.strip("[").strip("]").split(",")])


# def convert_string_to_int_array(s: str) -> np.ndarray:
#     return np.array([int(i) for i in s.strip("[").strip("]").split(",")])


df["fftdata"] = df["fftdata"].apply(convert_string_to_float_array)
# df["fftfreq"] = df["fftfreq"].apply(convert_string_to_float_array)
# df["fftindex"] = df["fftindex"].apply(convert_string_to_int_array)


# Preprocess the data set
def freq_to_order_normalization(row: pd.Series,
                                samples_n:int,
                                order_n_thd: int = 10,
                                fftdata_colname: str = 'fftdata',
                                rps_colname: str = 'fftspeed'):

    n_samples_slow = np.int16(row[rps_colname] * order_n_thd)
    y = row[fftdata_colname][:n_samples_slow]
    x = np.arange(n_samples_slow)
    x_new = np.linspace(0, n_samples_slow, num=samples_n, endpoint=True)
    return np.interp(x_new, x, y)

def execute_spectra_processing(df: pd.DataFrame,
                               order_n_thd: int = 10,
                               rps_colname: str = 'fftspeed',
                               fftdata_colname: str = 'fftdata'):
    max_rps = df[rps_colname].max()
    samples_n = np.int16(order_n_thd * max_rps)
    df_order_normalized = df.copy()
    df_order_normalized[fftdata_colname] = df_order_normalized.apply(lambda row: freq_to_order_normalization(row=row, samples_n=samples_n), axis=1)
    return df_order_normalized


df_normalized = df
# df_normalized = execute_spectra_processing(df, order_n_thd=10)

# Split data set into training, validation, and test
print("shape of data is:", df.shape)

train, test = train_test_split(df_normalized, test_size=0.2)
train, validation = train_test_split(df_normalized, test_size=0.2)

# Create local output directories
try:
    os.makedirs("/opt/ml/processing/output/train")
    os.makedirs("/opt/ml/processing/output/validation")
    os.makedirs("/opt/ml/processing/output/test")
    print("Successfully created directories")
except Exception as e:
    # if the Processing call already creates these directories (or directory otherwise cannot be created)
    print(e)
    print("Could Not Make Directories")
    pass

# Save data locally
try:
    train.to_parquet("/opt/ml/processing/output/train/train.parquet.gzip", compression='gzip')
    validation.to_parquet("/opt/ml/processing/output/validation/validation.parquet.gzip", compression='gzip')
    test.to_parquet("/opt/ml/processing/output/test/test.parquet.gzip", compression='gzip')
    print("Files Successfully Written")
except Exception as e:
    print("Could Not Write the Files")
    print(e)
    pass


print("Finished running processing job")