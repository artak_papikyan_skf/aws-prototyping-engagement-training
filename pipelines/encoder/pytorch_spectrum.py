import argparse
import json
import logging
import os
import sys
import time
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import torch
from torch import nn
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import Adam
import matplotlib.pyplot as plt
import seaborn as sns
from smexperiments.experiment import Experiment
from smexperiments.trial import Trial
from smexperiments.trial_component import TrialComponent

from smexperiments.search_expression import Filter, Operator, SearchExpression

#came from sagemaker experiment
from smexperiments.tracker import Tracker
tracker = Tracker.load()


# from imblearn.over_sampling import SMOTE
from sklearn.metrics import confusion_matrix
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

def get_prediction_all_samples(model, x, batch_size):
    dataset = TensorDataset(torch.tensor(x.astype(np.float32)))
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False)
    predictions = []
    for (x_batch, ) in data_loader:
        x_batch = x_batch.to(device)
        preds = model(x_batch)
        predictions.append(preds.detach().cpu().numpy())
    return np.concatenate(predictions)

def model_fn(model_dir):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Autoencoder(792)
    with open(os.path.join(model_dir, "model.pth"), "rb") as f:
        model.load_state_dict(torch.load(f))
    return model.to(device)

def predict_fn(input_data, model):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)
    model.eval()
    with torch.no_grad():
        return inference(model, input_data.to(device))
    

def inference(model:object,
             encoder_input_sample:np.ndarray,
             binarization_thd:float=10):
    encoder_output_sample = model(encoder_input_sample)
    #from tensor to np.array
    encoder_output_sample = encoder_output_sample.detach().cpu().numpy()
    #diff_score_per_sample, np_norm_1d_arr
    diff_encoder_in_out = diff_score_per_sample(encoder_input_sample.detach().cpu().numpy(), encoder_output_sample, np_norm_1d_arr, abs_flag=True)
    return (diff_encoder_in_out > binarization_thd).astype(np.int_)


def preparation_vsa_autoencoder_input(df:pd.DataFrame,
                                      ski_slope_thd:int = 8,
                                      spectrum_colname:str = 'fftdata',
                                      len_spectrum_name:str = 'fftn',
                                      label_colname:str = 'status')->dict:
    #check the len of fftdata, if we have diff length, we have to normalize by min:
#     n_differ_len_of_fftdata = df[len_spectrum_name].unique().size
#     fft_min_len = int(df[len_spectrum_name].min())
#     print(fft_min_len)
#     if n_differ_len_of_fftdata > 1:
#         df_copy = df.copy()
#         df_copy[spectrum_colname] = df[spectrum_colname].apply(lambda row: row[ski_slope_thd:fft_min_len])
#         df = df_copy
        
        
    array = df[[spectrum_colname, label_colname]].values

    X = np.stack(array[:,0])
    y = array[:,1].astype(int)

    return {'X':X, 'y':y}


def train(device,
          args)->list:
    #1. Train data
    df_train = pd.read_parquet(args.training_dir + '/train.parquet.gzip')
    print("df_train", df_train.shape, df_train.columns)
    input_train = preparation_vsa_autoencoder_input(df_train)
    x_train = input_train ['X']
    y_train = input_train ['y']
#     x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.2, shuffle=True)
    #2. Test data
    df_test = pd.read_parquet(args.test_dir + '/test.parquet.gzip')
    print("df_test", df_test.shape, df_test.columns)
    input_test = preparation_vsa_autoencoder_input(df_test)
    x_test = input_test ['X']
    y_test = input_test ['y']
    sample_size = x_train[0].size
    criterion = nn.MSELoss()
    # 1. Train data oversampling
#     if args.smote_flag:
#          oversample = SMOTE()
#          x_train, y_train = oversample.fit_resample(args.x_train, args.y_train)
    # 1. Train data preparation
    objective_label = 1#0-default
    train_in_distribution = x_train[y_train == objective_label]
    train_in_distribution = torch.tensor(train_in_distribution.astype(np.float32))
    train_in_dataset = TensorDataset(train_in_distribution)
    train_in_loader = DataLoader(train_in_dataset, batch_size=args.batch_size, shuffle=True)
    #Test data preparation
    test_dataset = TensorDataset(torch.tensor(x_test.astype(np.float32)))
    test_loader = DataLoader(test_dataset, batch_size=args.batch_size, shuffle=False)
    # 2. Model initialization
    model = Autoencoder(sample_size,
                       dropout_fraction = args.dropout_fraction,
                       neurons_n_layer_ratio = args.neurons_n_layer_ratio,
                       leaky_re_lu_slope = args.leaky_re_lu_slope).to(device)
    optimizer = Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    # 3. Training of model
    epoch = 0
    running_loss = args.running_loss
    while (running_loss > args.loss_thd) and (epoch < args.thd_epoch):
        running_loss = 0
        counter  = 0
        for (x_batch, ) in train_in_loader:
            counter += 1
            x_batch = x_batch.to(device)
            output = model(x_batch)
            loss = criterion(output, x_batch)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
        epoch_loss = running_loss / counter
        # your training logic and calculate accuracy and loss
        test_runing_loss = validate(model, test_loader, criterion)
        
        tracker.log_metric(metric_name='train loss', value=epoch_loss, iteration_number=epoch)
        tracker.log_metric(metric_name='test loss', value=test_runing_loss, iteration_number=epoch)
        
        metrics = test(model, device, x_test, y_test, batch_size=args.batch_size, iteration_number=epoch)
        
        print("epoch [{}/{}], train loss:{:.4f}, test loss:{:.4f}".format(epoch+1, args.thd_epoch, epoch_loss, test_runing_loss))
        logger.info(
                    "epoch [{}/{}], train loss:{:.4f}, test loss:{:.4f}".format(epoch+1, args.thd_epoch, epoch_loss, test_runing_loss))
        epoch += 1
#     metrics = test(model, device, x_test, y_test, batch_size=args.batch_size)
    return [model, metrics]
    # save_model(model, args.model_dir)
    
def get_diff_score_all_samples(test:np.ndarray,
                              pred:np.ndarray,
                              return_function=None,
                              scaling_function=None,
                              sklearn_metrics=None,
                              abs_flag:bool=False,
                              sklearn_metrics_flag:bool=False)->np.ndarray:
    n = np.size(test,0)
    result = np.zeros(n)
    if sklearn_metrics_flag:
        for i in range(n):
            result[i] = sklearn_metrics(test[i], pred[i])
    else:
        for i in range(n):
            result[i] = return_function(pred[i], test[i], scaling_function, abs_flag)
    return result
    
def diff_score_per_sample(test:np.ndarray,
                          pred:np.ndarray,
                         scaling_function,
                         abs_flag:bool=False)->float:
    if abs_flag:
        return np.sum(np.abs(scaling_function(pred) - scaling_function(test)))
    else:
        return np.sum(scaling_function(pred) - scaling_function(test))
        
def np_norm_1d_arr(arr:np.ndarray)->np.ndarray:
    return arr / np.linalg.norm(arr)
    
def validate(model, test_loader, loss_function, custom_flag:bool=False):
    print('Validating')
    model.eval()
    running_loss = 0.0
    test_loss = 0
    counter = 0
    with torch.no_grad():
        for (x_batch, ) in test_loader:
            counter += 1
            x_batch = x_batch.to(device)
            output = model(x_batch)
            loss = loss_function(output, x_batch)
            if custom_flag:
                running_loss += loss
            else:
                running_loss += loss.item()
    return  running_loss / counter
    
def test(model, device,
         x_test:np.ndarray,
         y_test:np.ndarray,
         batch_size:int = 100,
         thd_recall:float = 0.9,
         account_ISO:bool = False,
         iteration_number:int = 0)->dict:
    #ISO velocity thds
    vel_mmps_thds = np.array([0.762, 2.54, 7.62, 17.78]) / np.sqrt(2)
    result = {}
   #5. Result analysis
    x_pred = get_prediction_all_samples(model, x_test, batch_size)
    events_score = get_diff_score_all_samples(x_pred, x_test, diff_score_per_sample, np_norm_1d_arr, abs_flag=True)
    events_score_1_idx = np.where(y_test == 1)
    events_score_1 = events_score[events_score_1_idx]
    events_score_1_spectra = x_test[events_score_1_idx]
    # n_positive = events_score_1.size
    #Option1: imbalance case:
    events_score_0_idx = np.where(y_test == 0)
    events_score_0 = events_score[events_score_0_idx]
    events_score_0_spectra = x_test[events_score_0_idx]
    # ax_negative = sns.ecdfplot(events_score_0, color='red')
    ax_positive = sns.ecdfplot(events_score_1, color='green')
    for curve in ax_positive.lines:
            events_score_1_cumsum_x = curve.get_xdata()
            events_score_1_cumsum_y = curve.get_ydata()
    #6. Metrics computation
    #1. Find thd > 90% of samples
    
    thd_values = np.min(events_score_1_cumsum_x[np.where(events_score_1_cumsum_y > thd_recall)])
    
    # print('Recall =  %1.2f, Thd_difference:%1.2f'%(thd_coeff, thd_values))
    #Negative analysis:
    #FP
    fp_indexes = np.where(events_score_0<thd_values)
    fp_spectra = events_score_0_spectra[fp_indexes]
    fp = np.size(fp_spectra,0)
    #TN
    tn_indexes = np.where(events_score_0>thd_values)
    tn_spectra = x_test[tn_indexes]
    tn = np.size(tn_spectra,0)
    #Positive analysis:
    #TP
    tp_indexes = np.where(events_score_1<thd_values)
    tp_spectra = events_score_1_spectra[tp_indexes]
    tp = np.size(tp_spectra,0)
    fn = np.size(events_score_1,0) - tp
    #2. Account of case when negative labeled spectrum has spike with magnitude > than accptable level according ISO standard
    if account_ISO:
        fp_spectra_max = np.array(list(map(np.max, fp_spectra)))
        fp_indexes_excessive = np.where(fp_spectra_max > vel_mmps_thds[1])
        fp_spectra_excessive = fp_spectra[fp_indexes_excessive]
        tp = tp + np.size(fp_spectra_excessive, 0)
        fp = fp - np.size(fp_spectra_excessive, 0)
    print('Sanity check:',tp, fn, fp, tn, events_score_0.size+events_score_1.size, np.sum([tp, fn, fp, tn]))
    precision = tp / (tp+fp)
    recall    = tp / (tp+fn)
    specificity = tn / (tn+fp)
    f1_score = 2 * (precision * recall) / (precision + recall)
    result['precision'] = precision
    result['recall'] = recall
    result['specificity'] = specificity
    result['thd_values'] = thd_values
    logger.info("Test set: Precision: {:.3f}, Recall: {:.3f}, F1: {:.3f}, Specificity: {:.3f} \n".format(precision, recall, f1_score, specificity))
    
    tracker.log_metric(metric_name='test Recall', value=recall, iteration_number=iteration_number)
    tracker.log_metric(metric_name='test Precision', value=precision, iteration_number=iteration_number)
    tracker.log_metric(metric_name='test Specificity', value=specificity, iteration_number=iteration_number)
    
    return result
    
def save_model(model, model_name, args):
    logger.info("Saving the model.")
    path = os.path.join(args.model_dir, model_name)
    # recommended way from http://pytorch.org/docs/master/notes/serialization.html
    torch.save(model.cpu().state_dict(), path)
    
class Autoencoder(nn.Module):
    def __init__(self, 
                 input_size:int, 
                 dropout_fraction:float = 0.5,
                 neurons_n_layer_ratio:float = 2,
                 leaky_re_lu_slope:float = 0.2):
        self.dropout_fraction = dropout_fraction
        self.input_size = input_size
        self.neurons_n_layer_ratio = neurons_n_layer_ratio
        self.leaky_re_lu_slope = leaky_re_lu_slope
        super(Autoencoder, self).__init__()
        #Defining of neurons number per layer's in/out
        out_first = int(self.input_size/np.power(self.neurons_n_layer_ratio,1))
        out_second = int(self.input_size/np.power(self.neurons_n_layer_ratio,2))
        out_third = int(self.input_size/np.power(self.neurons_n_layer_ratio,3))
        out_forth = int(self.input_size/np.power(self.neurons_n_layer_ratio,4))
        self.encoder = nn.Sequential(
            nn.Linear(self.input_size, out_first), #input = 800, latency space = 50 neurons
            nn.Dropout(self.dropout_fraction),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_first, out_second),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_second, out_third),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_third, out_forth),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            )
        self.decoder = nn.Sequential(
            nn.Linear(out_forth, out_third),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_third, out_second),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_second, out_first),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            nn.Linear(out_first, self.input_size),
            nn.Dropout(self.dropout_fraction),
            nn.LeakyReLU(self.leaky_re_lu_slope),
            )
    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
    
if __name__ == "__main__":
    if torch.cuda.is_available():
      device = torch.device("cuda")
    else:
      device = torch.device("cpu")
    parser = argparse.ArgumentParser()
    # Data and model checkpoints directories
    parser.add_argument(
        "--batch-size",
        type=int,
        default=100,
        metavar="N",
        help="input batch size for training (default: 300)",
    )
    parser.add_argument(
        "--thd_epoch",
        type=int,
        default=2000,
        metavar="N",
        help="thd for number of epochs to train (default: 20)",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=2000,
        metavar="N",
        help="number of epochs to train (default: 10)",
    )
    parser.add_argument(
        "--lr", type=float, default=0.001, metavar="LR", help="learning rate (default: 0.001)"
    )
    parser.add_argument(
        "--dropout_fraction", type=float, default=0.5, metavar="dropout", help="dropout_fraction(default: 0.5)"
    )
    parser.add_argument(
        "--neurons_n_layer_ratio", type=float, default=2, metavar="neurons_ratio", help="neurons_n_in_out_layer_ratio(default: 2)"
    )
    parser.add_argument(
        "--leaky_re_lu_slope", type=float, default=0.2, metavar="leakyReLu", help="leakyReLu negative slope(default: 0.2)"
    )
    parser.add_argument(
        "--loss_thd", type=float, default=0.0001, metavar="loss_thd", help="loss_thd for early stop (default: 0.001)"
    )
    parser.add_argument("--running_loss", type=int, default=30, metavar="rl", help="start loss for  (default: 1)")
    parser.add_argument(
        "--objective_label",
        type=int,
        default=1,
        metavar="N",
        help="objective label for model training",
    )
    parser.add_argument(
        "--smote_flag",
        type=str,
        default=True,
        help="synthetic oversampling flag",
    )
    parser.add_argument(
        "--weight_decay", type=float, default = 1e-5, metavar="WD", help="weight_decay (default: 1e-5)")
    parser.add_argument(
        "--backend",
        type=str,
        default=None,
        help="backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)",
    )
    # Container environment
    parser.add_argument("--hosts", type=list, default=json.loads(os.environ["SM_HOSTS"]))
    parser.add_argument("--current-host", type=str, default=os.environ["SM_CURRENT_HOST"])
    parser.add_argument("--model-dir", type=str, default=os.environ["SM_MODEL_DIR"])
    parser.add_argument("--training_dir", type=str, default=os.environ["SM_CHANNEL_TRAINING"])
    parser.add_argument("--test_dir", type=str, default=os.environ["SM_CHANNEL_TEST"])
    parser.add_argument("--num-gpus", type=int, default=os.environ["SM_NUM_GPUS"])
    args = parser.parse_args()
    
    [model, metrics] = train(device, args)
    
#     group_idx = 23
#     model_dir = './'
    #3 Save of model
    with open(os.path.join(args.model_dir, 'model.pth'), 'wb') as f:
        torch.save(model.state_dict(), f)
    with open(os.path.join(args.model_dir, "bin_thd_test_metrics.json"), "w") as outfile:
        json.dump(metrics, outfile)
#     save_model(model, model_name, args)
#     epoch = 0
#     running_loss = args.running_loss
#     while (running_loss > args.loss_thd) and (epoch < args.thd_epoch):
#         running_loss = 0
#         for (x_batch, ) in train_in_loader:
#             x_batch = x_batch.to(device)
#             output = model(x_batch)
#             loss = criterion(output, x_batch)
#             optimizer.zero_grad()
#             loss.backward()
#             optimizer.step()
#             running_loss += loss.item()
#         print("epoch [{}/{}], train loss:{:.4f}".format(epoch+1, args.thd_epoch, running_loss))
#         epoch += 1
#     logger.info("epoch [{}/{}], train loss:{:.4f}".format(epoch+1, args.thd_epoch, running_loss))
    # test(model, test_loader, device)
    # save_model(model, args.model_dir)
    #Test of model + metrics for test + thd for binaryzation