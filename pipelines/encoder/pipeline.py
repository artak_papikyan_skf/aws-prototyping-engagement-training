import os

import boto3
import sagemaker
import sagemaker.session

from sagemaker.inputs import TrainingInput
from sagemaker.processing import (
    ProcessingOutput,
    ScriptProcessor,
)
from sagemaker.workflow.parameters import (
    ParameterInteger,
    ParameterString,
)
from sagemaker.workflow.pipeline import Pipeline
from sagemaker.workflow.steps import (
    ProcessingStep,
    TrainingStep,
    CacheConfig,
    TuningStep
)
from sagemaker.workflow.step_collections import RegisterModel
from sagemaker.workflow.functions import Join
from sagemaker.pytorch import PyTorch
from sagemaker.tuner import (
    ContinuousParameter,
    CategoricalParameter,
    HyperparameterTuner,
)

BASE_DIR = os.path.dirname(os.path.realpath(__file__))


def get_sagemaker_client(region):
    """Gets the sagemaker client.

       Args:
           region: the aws region to start the session
           default_bucket: the bucket to use for storing the artifacts

       Returns:
           `sagemaker.session.Session instance
       """
    boto_session = boto3.Session(region_name=region)
    sagemaker_client = boto_session.client("sagemaker")
    return sagemaker_client


def get_session(region, default_bucket):
    """Gets the sagemaker session based on the region.

    Args:
        region: the aws region to start the session
        default_bucket: the bucket to use for storing the artifacts

    Returns:
        `sagemaker.session.Session instance
    """

    boto_session = boto3.Session(region_name=region)

    sagemaker_client = boto_session.client("sagemaker")
    runtime_client = boto_session.client("sagemaker-runtime")
    return sagemaker.session.Session(
        boto_session=boto_session,
        sagemaker_client=sagemaker_client,
        sagemaker_runtime_client=runtime_client,
        default_bucket=default_bucket,
    )


def get_pipeline_custom_tags(new_tags, region, sagemaker_project_arn=None):
    try:
        sm_client = get_sagemaker_client(region)
        response = sm_client.list_tags(
            ResourceArn=sagemaker_project_arn)
        project_tags = response["Tags"]
        for project_tag in project_tags:
            new_tags.append(project_tag)
    except Exception as e:
        print(f"Error getting project tags: {e}")
    return new_tags


def get_pipeline(
        region,
        sagemaker_project_arn=None,
        role=None,
        default_bucket=None,
        model_package_group_name="encoder-tuning-model-packages-artak",
        pipeline_name="encoder-pipeline-artak-proj",
        base_job_prefix="artak/encoder-tuning-artak",
):
    """Gets a SageMaker ML Pipeline instance working with on training data.

    Args:
        region: AWS region to create and run the pipeline.
        role: IAM role to create and run steps and pipeline.
        default_bucket: the bucket to use for storing the artifacts

    Returns:
        an instance of a pipeline
    """
    sagemaker_session = get_session(region, default_bucket)
    if role is None:
        role = sagemaker.session.get_execution_role(sagemaker_session)
    default_bucket = sagemaker_session.default_bucket()
    model_bucket_key = f"{default_bucket}/{base_job_prefix}/EncoderTuning/models"
    model_path = f"s3://{model_bucket_key}"

    processing_instance_count = ParameterInteger(
        name="ProcessingInstanceCount", default_value=1
    )
    processing_instance_type = ParameterString(
        name="ProcessingInstanceType", default_value="ml.m5.xlarge"
    )
    training_instance_type = ParameterString(
        name="TrainingInstanceType", default_value="ml.m5.xlarge"
    )
    model_approval_status = ParameterString(
        name="ModelApprovalStatus", default_value="PendingManualApproval"
    )
    tuner_max_jobs_count = ParameterInteger(
        name="TunerMaxJobsCount", default_value=3
    )
    tuner_max_paralel_jobs_count = ParameterInteger(
        name="TunerMaxParalelJobsCount", default_value=3
    )
    tuning_group_id = ParameterString(
        name="TuningGroupId", default_value="vsa_group_23"
    )
    model_path = ParameterString(
        name="ModelPath", default_value=model_path
    )
    model_path_register = ParameterString(
        name="ModelPathRegister", default_value=model_bucket_key
    )
    event_time = ParameterString(name='EventTime', default_value='1.638725625E9')
    cache_config = CacheConfig(enable_caching=True, expire_after="30d")

    image_uri = '676788233673.dkr.ecr.eu-west-1.amazonaws.com/sagemaker-studio-d-49bmfpyxfcph:artak-papikyan'

    script_processor = ScriptProcessor(command=['python3'],
                                       image_uri=image_uri,
                                       role=role,
                                       instance_count=1,
                                       instance_type='ml.m5.xlarge')

    default_prefix = 'artak'
    processing_output_location = f's3://{default_bucket}/{default_prefix}/spectrum_processing'
    code=os.path.join(BASE_DIR,"spectrum_processing.py"), 
    step_process = ProcessingStep(
        name="EncoderProcess",
        processor=script_processor,
        code=os.path.join(BASE_DIR,"spectrum_processing.py"), 
        outputs=[
            ProcessingOutput(
                output_name='train',
                source='/opt/ml/processing/output/train',
                destination=Join(
                    on="/",
                    values=[
                        processing_output_location,
                        tuning_group_id,
                        event_time,
                        "train",
                    ])),
            ProcessingOutput(
                output_name='validation',
                source='/opt/ml/processing/output/validation',
                destination=Join(
                    on="/",
                    values=[
                        processing_output_location,
                        tuning_group_id,
                        event_time,
                        "validation",
                    ])),
            ProcessingOutput(
                output_name='test',
                source='/opt/ml/processing/output/test',
                destination=Join(
                    on="/",
                    values=[
                        processing_output_location,
                        tuning_group_id,
                        event_time,
                        "test",
                    ]))
        ],
        job_arguments=['--region', region, '--group_id', tuning_group_id, '--event_time', event_time],
        cache_config=cache_config
    )
    

    prefix = 'aws_prototyping/artak/output/'
    estimator = PyTorch(entry_point=os.path.join(BASE_DIR,'pytorch_spectrum.py'),
                    role=role,
                    py_version='py3',
                    framework_version='1.8.1',
                    instance_count=1,
                    output_path=model_path,    
                    instance_type='ml.c5.2xlarge',
                    hyperparameters={
                        'epochs': 2,
                        'backend': 'gloo'
                    },
                   max_wait=86400,
                   use_spot_instances=True,
                   base_job_name="pytorch-spectrum",
                   checkpoint_s3_uri=f"s3://{default_bucket}/{prefix}")
    
    objective_metric_name = 'test_precision'
    objective_type = "Maximize"
    metric_definitions = [{
                            "Name": "test_precision", 
                            "Regex": "Precision: ([0-9\\.]+)"
                          }]

    hyperparameter_ranges = {
        "lr": ContinuousParameter(0.0005, 0.01),
        "batch-size": CategoricalParameter([50, 100, 200, 300, 500]),
        "dropout_fraction":ContinuousParameter(0.1, 0.5),
        "neurons_n_layer_ratio":ContinuousParameter(1.5, 3),
        "leaky_re_lu_slope":ContinuousParameter(0.1, 0.5)
    }

    tuner_log = HyperparameterTuner(
        estimator,
        objective_metric_name,
        hyperparameter_ranges,
        metric_definitions,
        max_jobs=4,
        max_parallel_jobs=2,
        objective_type=objective_type,
    )

    step_tuning = TuningStep(
        name="EncoderTuning",
        tuner=tuner_log,
        inputs = {'training':  TrainingInput(
                s3_data=step_process.properties.ProcessingOutputConfig.Outputs[
                    "train"
                ].S3Output.S3Uri,
                content_type="text/csv"),
                "test": TrainingInput(
                s3_data=step_process.properties.ProcessingOutputConfig.Outputs[
                    "test"
                ].S3Output.S3Uri,
                content_type="text/csv",
            ),},
        cache_config=cache_config,
    )
    
    step_register_best = RegisterModel(
        name="RegisterBestEncoderModel",
        estimator=estimator,
        image_uri = '763104351884.dkr.ecr.eu-west-1.amazonaws.com/pytorch-inference:1.8.1-cpu-py3',
        model_data=step_tuning.get_top_model_s3_uri(top_k=0, s3_bucket=model_path_register),
        content_types=["application/x-npy"],
        response_types=["application/x-npy"],
        inference_instances=["ml.m5.large"],
        transform_instances=["ml.m5.large"],
        model_package_group_name=model_package_group_name,
        approval_status=model_approval_status
    )    
    
    pipeline = Pipeline(
        name=pipeline_name,
        parameters=[
            processing_instance_type,
            processing_instance_count,
            training_instance_type,
            model_approval_status,
            tuner_max_jobs_count,
            tuner_max_paralel_jobs_count,
            tuning_group_id,
            event_time,
            model_path_register,
            model_path
        ],
        steps=[
            step_process,
            step_tuning,
            step_register_best
        ],
        sagemaker_session=sagemaker_session,
    )
    
    return pipeline
